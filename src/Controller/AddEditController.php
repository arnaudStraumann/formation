<?php

namespace App\Controller;

use App\Entity\Module;
use App\Entity\Categorie;
use App\Entity\Formation;
use App\Entity\Stagiaire;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// TODO : forms!!
class AddEditController extends AbstractController
{
    /**
     * @Route("/add/formation", name="addFormation")
     * @Route("/edit/formation/{id}", name="editFormation")
     */
    public function addEditFormation(Formation $formation, ObjectManager $manager, Request $request)
    {
        // TO DO
        $add=false;

        if(!$formation) {
            $add = true;
            $formation = new Formation();
            $form = $this->createFormBuilder($formation)
            ->add('nom',TextType::class)
            ->add('date_debut',DateType::class)
            ->add('date_fin', DateType::class)
            ->add('Valider', SubmitType::class)
            ->getForm();
        }
 
        else {
        $form = $this->createFormBuilder($formation)
        ->add('title',TextType::class)
        ->add('Valider', SubmitType::class)
        ->getForm();
         }
 
         $form->handleRequest($request);
               
         if($form->isSubmitted() && $form->isValid()) {
             if($add) {
                 $post = new Post();
                 $post->setAuthor($user);
                 $post->setformation($formation);
 
                 $post->setContent($form->get('content')->getData());
                 $manager->persist($post);
             }
 
             $manager->persist($formation);
             $manager->flush();
 
             return $this->redirectToRoute('formation_show', ['id' => $formation->getID()]);
         }

        return $this->render('add_edit/index.html.twig', ['form' => $form->createView(),
            'title' => 'Modification de la formation '.$formation, 'editMode' => $formation->getId() != null, 'formation' => $formation
        ]);
    }

    /**
     * @Route("/add/stagiaire", name="addStagiaire")
     * @Route("/edit/stagiaire/{id}", name="editStagiaire")
     */
    public function addEditStagiaire(Stagiaire $stagiaire, ObjectManager $manager, Request $request)
    {

        return $this->render('add_edit/addEdit.html.twig', ['form' => $form->createView(),
        'title' => 'Modification du stagiaire '.$stagiaire, 'editMode' => $stagiaire->getId() != null, 'stagiaire' => $stagiaire
        ]);
    }

    /**
     * @Route("/add/module", name="addModule")
     * @Route("/edit/module/{id}", name="editModule")
     */
    public function addEditModule(Module $module, ObjectManager $manager, Request $request)
    {

        return $this->render('add_edit/addEdit.html.twig', ['form' => $form->createView(),
            'title' => 'Modification du module '.$module, 'editMode' => $module->getId() != null, 'module' => $module
        ]);
    }

    /**
     * @Route("/add/categorie", name="addCategorie")
     * @Route("/edit/categorie/{id}", name="editCategorie")
     */
    public function addEditCategorie(Categorie $categorie, ObjectManager $manager, Request $request)
    {

        return $this->render('add_edit/addEdit.html.twig', ['form' => $form->createView(),
            'title' => 'Modification de la catégorie '.$categorie, 'editMode' => $categorie->getId() != null, 'categorie' => $categorie
        ]);
    }
}
