<?php

namespace App\Controller;

use App\Entity\Module;
use App\Entity\Formation;
use App\Entity\Stagiaire;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ShowController extends AbstractController
{
    /**
     * @Route("/", name="showListeFormations")
     */
    public function showListeFormations() {
        $all_formations = $this->getDoctrine()->getRepository(Formation::class)->findAll();

        return $this->render('show/listeFormations.html.twig', [
            'title' => 'Liste des formations',
            'formations' => $all_formations
        ]);
    }

    /**
     * @Route("/show/infoStagiaire/{id}", name="showInfoStagiaire")
     */
    public function showInfoStagiaire(Stagiaire $stagiaire, ObjectManager $manager, Request $request) {
        return $this->render('show/infoStagiaire.html.twig', [
            'title' => 'Informations du stagiaire '.$stagiaire,
            'stagiaire' => $stagiaire
        ]);
    }

    /**
     * @Route("/show/listeAllModules", name="showListeAllModules")
     */
    public function showListeAllModules() {
        $all_modules = $this->getDoctrine()->getRepository(Module::class)->findAll();

        return $this->render('show/listeAllModules.html.twig', [
            'title' => 'Mode Admin - Liste de tous les modules',
            'modules' => $all_modules
        ]);
    }

    /**
     * @Route("/show/listeModulesSession/{id}", name="showListeModulesSession")
     */
    public function showListeModulesSession(Formation $formation, ObjectManager $manager, Request $request)
    {
        return $this->render('show/listeModulesSession.html.twig', [
            'title' => 'Liste des modules de la formation '.$formation,
            'formation' => $formation
        ]);
    }

    /**
     * @Route("/show/listeStagiairesSession{id}", name="showListeStagiairesSession")
     */
    public function showListeStagiairesSession(Formation $formation, ObjectManager $manager, Request $request)
    {
        return $this->render('show/listeStagiairesSession.html.twig', [
            'title' => 'Liste des stagiaires de la formation '.$formation,
            'formation' => $formation
        ]);
    }
}
